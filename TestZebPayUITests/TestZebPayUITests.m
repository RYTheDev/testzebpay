//
//  TestZebPayUITests.m
//  TestZebPayUITests
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreData/CoreData.h>

@interface TestZebPayUITests : XCTestCase

@property XCUIApplication *app;

@end

@implementation TestZebPayUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    
    self.app = [[XCUIApplication alloc] init];
    
    [self.app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testInitialView{
    
    BOOL loaderExists = [self.app.staticTexts[@"loadingLbl"] exists];
    
    BOOL collectionViewExists = self.app.collectionViews[@"ourCollectionView"].exists;
    
    // Either loader or collection view should be visible
    XCTAssertTrue(loaderExists || collectionViewExists);
    
    [self collectionViewTest];
}

-(void)collectionViewTest{
    
    XCTAssertTrue(self.app.collectionViews[@"ourCollectionView"].cells.count > 0);
    
    // Image
    BOOL imageLoaderExists = [self.app.collectionViews[@"ourCollectionView"] cells].staticTexts[@"loadingLbl"].exists;
    BOOL imageExists = [self.app.collectionViews[@"ourCollectionView"] cells].images[@"imageView"].exists;
    
    XCTAssertTrue(imageLoaderExists || imageExists);
    
    // Artist name
XCTAssertTrue(self.app.collectionViews[@"ourCollectionView"].cells.staticTexts[@"artistName"].exists);
    
    // Genres
XCTAssertTrue(self.app.collectionViews[@"ourCollectionView"].cells.staticTexts[@"genresLbl"].exists);
}



@end
