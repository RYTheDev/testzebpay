//
//  main.m
//  TestZebPay
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
