//
//  Entity+CoreDataProperties.m
//  TestZebPay
//
//  Created by Rahul Yadav on 15/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Entity+CoreDataProperties.h"

@implementation Entity (CoreDataProperties)

+ (NSFetchRequest<Entity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Entity"];
}

@dynamic artistName;
@dynamic genreNameList;
@dynamic image;
@dynamic imageURLStr;
@dynamic index;

@end
