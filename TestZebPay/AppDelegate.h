//
//  AppDelegate.h
//  TestZebPay
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

