//
//  CellCollectionViewCell.m
//  TestZebPay
//
//  Created by Rahul Yadav on 14/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import "Cell.h"

@interface Cell ()

@property (weak, nonatomic) IBOutlet UIImageView *ourImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *genresLbl;
@property (weak, nonatomic) IBOutlet UILabel *loadingLbl;

@end

@implementation Cell

-(void)configure:(Entity*)entity{

    // Image
    if(entity.image == nil){
        
        // Doesn't exist, lets download it
        
        self.loadingLbl.hidden = false;
        
        [self loadImage:entity];
    }
    else{
        
        self.loadingLbl.hidden = true;
        
        self.ourImageView.image = [UIImage imageWithData:entity.image];
    }
    
    self.nameLbl.text = entity.artistName;
    
    // Genre names
    
    NSArray *arrGenre = (NSArray*)entity.genreNameList;

    if(arrGenre.count == 0){
        
        return;
    }
    
    NSMutableString *mStr = [NSMutableString string];
    
    for (NSInteger index = 0; index < arrGenre.count; index++) {
        
        if(index == 0){
            
            [mStr appendString:@"Genres:"];
        }
        else{
            
            [mStr appendString:@","];
        }
        
        [mStr appendString:[NSString stringWithFormat:@" %@", [arrGenre objectAtIndex:index]]];
    }
    
    NSDictionary *existingAttr = [self.genresLbl.attributedText attributesAtIndex:0 effectiveRange:nil];
    UIFont *existingFont = [existingAttr objectForKey:NSFontAttributeName];
    
    NSMutableAttributedString *mAttrStr = [[NSMutableAttributedString alloc] initWithString:mStr attributes:existingAttr];
    [mAttrStr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:existingFont.pointSize] range:NSMakeRange(0, 7)];
    
    self.genresLbl.attributedText = mAttrStr;
}

-(void)loadImage:(Entity*)entity{
    
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:entity.imageURLStr] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(data){
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                entity.image = data;
                
                [self determineImageShow: entity];
            }];
        }
        
    }] resume];
}

// Check whether this cell is visible or not
-(void)determineImageShow: (Entity*)entity{
    
    // Find the collection view

    UIView *superView = self.superview;
    while (superView) {
        
        if([superView isKindOfClass:[UICollectionView class]]){
            
            UICollectionView *collectionView = (UICollectionView*)superView;
            
            NSIndexPath *ourIndexPath = [NSIndexPath indexPathForItem:entity.index inSection:0];
            
            if([[collectionView indexPathsForVisibleItems] containsObject: ourIndexPath]){
                
                // Currently visible
                
                Cell *ourCell = (Cell*)[collectionView cellForItemAtIndexPath:ourIndexPath];
                
                ourCell.loadingLbl.hidden = true;
                
                ourCell.ourImageView.image = [UIImage imageWithData:entity.image];
            }
            
            break;
        }
    }
}

@end
