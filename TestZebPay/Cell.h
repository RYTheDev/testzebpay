//
//  CellCollectionViewCell.h
//  TestZebPay
//
//  Created by Rahul Yadav on 14/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entity+CoreDataProperties.h"

@interface Cell : UICollectionViewCell

-(void)configure:(Entity*)entity;

@end
