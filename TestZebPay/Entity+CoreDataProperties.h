//
//  Entity+CoreDataProperties.h
//  TestZebPay
//
//  Created by Rahul Yadav on 15/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "Entity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Entity (CoreDataProperties)

+ (NSFetchRequest<Entity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *artistName;
@property (nullable, nonatomic, retain) NSObject *genreNameList;
@property (nullable, nonatomic, retain) NSData *image;
@property (nullable, nonatomic, copy) NSString *imageURLStr;
@property (nonatomic) int16_t index;

@end

NS_ASSUME_NONNULL_END
