//
//  ViewController.m
//  TestZebPay
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import "ViewController.h"
#import "ResponseModel.h"
#import "Entity+CoreDataProperties.h"
#import "AppDelegate.h"
#import "Cell.h"

/***** Macros *****/

#define kCellID         @"Cell"
#define kCellID1        @"Cell1"
#define kCellID2         @"Cell2"


@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property NSArray *arrEntity;
@property (weak, nonatomic) IBOutlet UICollectionView *ourCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *loadingLbl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self checkService];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Others

// Check if data doesn't exist then call service
-(void)checkService{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Entity"];
    request.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"index" ascending:true]];
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    NSError *error = nil;
    self.arrEntity = [appDelegate.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    if(self.arrEntity.count == 0){
        
        self.loadingLbl.hidden = false;
        
        [self initiateService];
    }
    else{
        
        [self.ourCollectionView reloadData];
        
        self.loadingLbl.hidden = true;
    } 
}

#pragma mark: Web services

-(void)initiateService{
    
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:@"https://rss.itunes.apple.com/api/v1/us/apple-music/new-music/10/explicit/json"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(data){

            NSArray *modelList = [ResponseModel returnModelList:data];
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self insertInCoreData:modelList];
            }];
        }
        
    }] resume];
}

#pragma mark: Core data

-(void)insertInCoreData: (NSArray*) list{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    NSMutableArray *mArrEntity = [NSMutableArray array];
    
    NSInteger index = 0;
    for(ResponseModel *model in list){
        
        Entity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:appDelegate.persistentContainer.viewContext];
        
        entity.index = index++;
        entity.artistName = model.artistName;
        entity.imageURLStr = model.imageURLStr;
        
        entity.genreNameList = model.arrGenreName;
        
        [mArrEntity addObject:entity];
    }
    
    [appDelegate saveContext];
    
    self.arrEntity = [NSArray arrayWithArray:mArrEntity];
    
    self.loadingLbl.hidden = true;
    
    [self.ourCollectionView reloadData];
}

#pragma mark: UICollectionview callbacks

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.arrEntity.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellID = nil;
    
    NSInteger cellIDType = indexPath.row % 3;
    switch (cellIDType) {
        case 0:
            cellID = kCellID;
            break;
        
        case 1:
            cellID = kCellID1;
            break;
            
        default:
            cellID = kCellID2;
            break;
    }
    
    Cell *cell = (Cell*)[collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    
    [cell configure:[self.arrEntity objectAtIndex:indexPath.row]];
    
    return  cell;
}

@end
