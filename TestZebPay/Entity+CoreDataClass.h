//
//  Entity+CoreDataClass.h
//  TestZebPay
//
//  Created by Rahul Yadav on 15/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface Entity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Entity+CoreDataProperties.h"
