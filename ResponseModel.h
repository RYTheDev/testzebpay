//
//  ResponseModel.h
//  TestZebPay
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResponseModel : NSObject

@property NSString *artistName;
@property NSString *imageURLStr;
@property NSArray *arrGenreName;

+(NSArray*)returnModelList: (NSData*) data;


@end
