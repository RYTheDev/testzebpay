//
//  ResponseModel.m
//  TestZebPay
//
//  Created by Rahul Yadav on 13/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import "ResponseModel.h"

@implementation ResponseModel

+(NSArray*)returnModelList: (NSData*) data{
    
    NSError *error = nil;
    
    NSDictionary *rootDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    if(error){
        
        return nil;
    }
    
    if (![rootDict isKindOfClass:[NSDictionary class]]){
        
        return nil;
    }
    
    NSMutableArray *mArrResult = [[NSMutableArray alloc] init];
    
    NSArray *results = [[rootDict objectForKey:@"feed"] objectForKey:@"results"];
    
    for (NSDictionary *result in results){
        
        ResponseModel *model = [ResponseModel returnModel:result];
        
        if (model){
            
            [mArrResult addObject:model];
        }
    }
    
    
    return [NSArray arrayWithArray:mArrResult];
}


+(ResponseModel*)returnModel: (NSDictionary*) dict{
    
    if (![dict isKindOfClass:[NSDictionary class]]){
        
        return nil;
    }
    
    ResponseModel *model = [[ResponseModel alloc] init];
    
    model.artistName = [dict objectForKey:@"artistName"];
    model.imageURLStr = [dict objectForKey:@"artworkUrl100"];

    NSMutableArray *mArrGenreName = [[NSMutableArray alloc] init];
    
    for (NSString *item in [dict objectForKey:@"genreNames"]) {
        
        [mArrGenreName addObject:item];
    }
    model.arrGenreName = [NSArray arrayWithArray:mArrGenreName];
    
    
    return model;
}

@end
