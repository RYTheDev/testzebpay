//
//  TestCoreData.m
//  TestZebPay
//
//  Created by Rahul Yadav on 14/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreData/CoreData.h>
#import "ResponseModel.h"
#import "Entity+CoreDataProperties.h"

@interface TestCoreData : XCTestCase

@property (nonatomic) NSPersistentContainer *persistentContainer;

@end

@implementation TestCoreData

-(NSPersistentContainer*)persistentContainer{
    
    if (!_persistentContainer) {
        
        _persistentContainer = [NSPersistentContainer persistentContainerWithName:@"TestZebPay"];
        
        NSPersistentStoreDescription *description = [[NSPersistentStoreDescription alloc] init];
        description.type = NSInMemoryStoreType;
        
        _persistentContainer.persistentStoreDescriptions = @[description];
    }
    
    return _persistentContainer;
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) testLoadInMemoryDB{
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"Loading the in memory store"];
    
    [self.persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription * _Nonnull storeDesc, NSError * _Nullable error) {
        
        XCTAssertNil(error, @"Couldn't load the DB in memory");
        
        [expectation fulfill];
        
        [self insertEntity];
    }];
    
    [self waitForExpectationsWithTimeout:120 handler:^(NSError * _Nullable error) {
        
        XCTAssertNil(error, @"Taking too much time to load in memory DB");
    }];
}

-(void)insertEntity{
    
    ResponseModel *model = [[ResponseModel alloc] init];
    
    model.artistName = @"Jay Z";
    model.imageURLStr = @"http://is2.mzstatic.com/image/thumb/Music117/v4/95/99/bc/9599bce1-003a-da13-5bea-b54ec5efa1c9/source/200x200bb.png";
    model.arrGenreName = @[@"Hip-Hop/Rap", @"Music"];
    
    Entity *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Entity" inManagedObjectContext:self.persistentContainer.viewContext];
    
    entity.index = 1;
    entity.artistName = model.artistName;
    entity.imageURLStr = model.imageURLStr;
    entity.genreNameList = model.arrGenreName;
    
    NSError *error = nil;
    [self.persistentContainer.viewContext save:&error];
    
    XCTAssertNil(error, @"Couldn't save the entity in DB");
    
    [self fetchEntity];
}

-(void)fetchEntity{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Entity"];
    
    NSError *error = nil;
    Entity *fetchedObject = [[self.persistentContainer.viewContext executeFetchRequest:request error:&error] firstObject];
        
    XCTAssertNotNil(fetchedObject, @"Couldn't fetch the inserted entity from DB");
}



- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
