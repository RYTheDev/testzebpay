//
//  TestWebService.m
//  TestZebPay
//
//  Created by Rahul Yadav on 14/07/17.
//  Copyright © 2017 RYTheDev. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ResponseModel.h"

@interface TestWebService : XCTestCase

@end

@implementation TestWebService

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testService {
    
    XCTestExpectation *expectation = [self expectationWithDescription:@"web service"];
    
    [[[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:@"https://rss.itunes.apple.com/api/v1/us/apple-music/new-music/10/explicit/json"] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if(data){
            
            NSArray *modelList = [ResponseModel returnModelList:data];
            
            XCTAssert(modelList.count > 0, @"Couldn't parse relevant data from server");
        }
        else{
            
            XCTAssertTrue(@"Couldn't recieve data from server");
        }
        
        [expectation fulfill];
        
    }] resume];
    
    [self waitForExpectationsWithTimeout:60 handler:^(NSError * _Nullable error) {
       
        XCTAssertNil(error, @"Timeout in web service");
    }];
}

@end
